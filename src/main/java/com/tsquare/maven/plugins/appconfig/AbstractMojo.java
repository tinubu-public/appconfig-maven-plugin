package com.tsquare.maven.plugins.appconfig;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.maven.plugins.annotations.Parameter;
import org.springframework.core.io.DefaultResourceLoader;

public abstract class AbstractMojo extends org.apache.maven.plugin.AbstractMojo {

   @Parameter(property = "project.compileClasspathElements", required = true, readonly = true)
   protected List<String> moduleCompileClasspath;

   protected DefaultResourceLoader resourceLoader() {
      return new DefaultResourceLoader(classLoader());
   }

   protected ClassLoader classLoader() {
      return Thread.currentThread().getContextClassLoader();
   }

   protected void loadModuleClasspath() {
      getLog().debug("Use classpath : " + String.join(",", moduleCompileClasspath));

      Set<URI> uris = new HashSet<>();
      for (String element : moduleCompileClasspath) {
         uris.add(new File(element).toURI());
      }

      ClassLoader contextClassLoader =
            URLClassLoader.newInstance(uris.stream().map(this::toURL).toArray(URL[]::new), classLoader());

      Thread.currentThread().setContextClassLoader(contextClassLoader);
   }

   protected URL toURL(URI uri) {
      try {
         return uri.toURL();
      } catch (MalformedURLException e) {
         throw new IllegalStateException(e);
      }
   }
}
