package com.tsquare.maven.plugins.appconfig.export.filters;

import java.util.Objects;
import java.util.function.Predicate;

public interface KeyFilter extends Predicate<String> {

   static KeyFilter identity() {
      return __ -> true;
   }

   @Override
   default KeyFilter and(Predicate<? super String> other) {
      Objects.requireNonNull(other);
      return t -> Predicate.super.and(other).test(t);
   }

   @Override
   default KeyFilter negate() {
      return t -> Predicate.super.negate().test(t);
   }

   @Override
   default KeyFilter or(Predicate<? super String> other) {
      Objects.requireNonNull(other);
      return t -> Predicate.super.or(other).test(t);
   }
}
